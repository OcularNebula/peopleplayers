import javax.swing.*;

public class PeoplePlayers{

	public static void main(String[] args){

	String numPeopleInput = JOptionPane.showInputDialog(null, "Enter the number of people");
	String numPlayersInput = JOptionPane.showInputDialog(null, "Enter the number of players");
	int numPeople = Integer.parseInt(numPeopleInput);
	int numPlayers = Integer.parseInt(numPlayersInput);
	int groupSize = 0;
	int teamSize = 0;

	if(numPeople > 10){
		groupSize = numPeople/2;
		}
	else if(numPeople >= 3){
		groupSize = numPeople/3;
		}
	else{
		JOptionPane.showMessageDialog(null, "The number of people has to be at least 3");
		}

	if((numPlayers > 11) && (numPlayers < 55)){
		teamSize = numPlayers/11;
		}
	else{
		teamSize = 1;
		}

	JOptionPane.showMessageDialog(null, "The number of people is " + numPeople + " and the group size is " + groupSize + "\n" + "The number of players is " + numPlayers + " and the team size is " + teamSize);
	}
}
